'''
taskarchive.py

Contains class TaskArchive which adds json files task_info.json and
hash_info.json to a zip archive. The hash information for all the files and
task_info can be validated by calling the validate function


:: Example Usage

    In [90]: with TaskArchive("d:\\temp.zip", "w") as _arch:
        ...:     _arch.write_dir("d:\\temp")


    In [91]: with TaskArchive("d:\\temp.zip", "r") as _arch:
        ...:     if not _arch.validate():
        ...:         print len(_arch.errors)
        ...:     else:
        ...:         print 'OK'
        ...:
    OK


TODO: Add more format awareness with functions like has_submission() or the
like


author: talha.ahmed@gmail.com
'''

import json
import zipfile
import os
import re
from collections import OrderedDict
from hashlib import md5
import logging


logger = logging.getLogger(__name__)


class FailedIntegrityCheck(Exception):
    pass


class NebulaArchive(zipfile.ZipFile):
    __hash_filename = 'hash_info.json'
    _NaCl = 'archive.nebulapipeline.com'

    def __init__(self, path, mode='r', compression=zipfile.ZIP_DEFLATED,
                 allowZip64=True, pwd=None):
        if mode not in ('r', 'w'):
            raise ValueError("Mode must be either 'w' or 'r'")
        super(NebulaArchive, self).__init__(
                path, mode=mode, compression=compression,
                allowZip64=allowZip64)
        self.__hash_info = None
        if pwd is not None:
            self.setpassword(pwd)
        self.errors = OrderedDict()
        self._closed = False

    @property
    def hash_info(self):
        if self.__hash_info is None:
            if self.mode in 'ra':
                data = self.read(self.__hash_filename)
                self.__hash_info = json.loads(data,
                                              object_pairs_hook=OrderedDict)
            else:
                self.__hash_info = OrderedDict()
        return self.__hash_info

    def __calculate_hash(self, name):
        zinfo = self.getinfo(name)
        _hash = md5()
        _hash.update(zinfo.filename)
        _hash.update("%04d-%02d-%02d %02d:%02d" % zinfo.date_time[:-1])
        _hash.update(hex(zinfo.CRC).strip('L'))
        _hash.update(hex(zinfo.file_size).strip('L'))
        _hash.update(self._NaCl)
        return _hash.hexdigest()

    def __calculate_hash_info(self):
        hash_info = OrderedDict()
        for name in self.namelist():
            if name == self.__hash_filename:
                continue
            hash_info[name] = self.__calculate_hash(name)
        self.__hash_info = hash_info

    def _write_hash_info(self):
        logger.info('Calculating hash info ...')
        self.__calculate_hash_info()
        logger.info('Writing hash info ...')
        self.writestr(
                self.__hash_filename, json.dumps(self.hash_info, indent=2))
        self.comment = self.__calculate_hash(self.__hash_filename)

    def close(self):
        self._closed = True
        if self.mode in ('w', 'a'):
            self._write_hash_info()
        super(NebulaArchive, self).close()

    def validate(self):
        self.errors.clear()

        # testing zip
        if self.testzip():
            self.errors[''] = zipfile.BadZipfile(
                    'The archive appears to have been corrupted')
            return False

        # check hash_info.json
        try:
            if self.comment != self.__calculate_hash(self.__hash_filename):
                raise FailedIntegrityCheck(
                        'The file "%s" failed integrity check'
                        % self.__hash_filename)
                return False
        except (KeyError, ValueError, FailedIntegrityCheck) as err:
            self.errors[self.__hash_filename] = err

        for name in self.namelist():
            if name == self.__hash_filename:
                continue
            if self.hash_info.get(name) != self.__calculate_hash(name):
                self.errors[name] = FailedIntegrityCheck(
                    'The file "%s" failed integrity check' % name)

        if self.errors:
            return False
        return True

    def check_validate(self):
        if not self.validate():
            raise list(self.errors.values())[0]
        return True

    def write_dir(self, directory, arch_dir=None):
        if self.mode not in 'wa':
            raise RuntimeError('Cannot write to zip file')
        for dirpath, dirnames, filenames in os.walk(directory):
            for filename in filenames + dirnames:
                full_path = os.path.join(dirpath, filename)
                arch_path = os.path.relpath(full_path, directory)
                if arch_dir is not None:
                    self.write_empty_dir(arch_dir)
                    arch_path = os.path.join(arch_dir, arch_path)
                logger.info('writing %s ...' % full_path)
                self.write(full_path, arch_path)

    def write_empty_dir(self, dirname):
        dirname = dirname.replace('\\', '/')
        splits = dirname.split('/')
        for num in range(1, len(splits)+1):
            folder = '/'.join(splits[:num]) + '/'
            try:
                self.getinfo(folder)
            except KeyError:
                self.write('/', folder)

    def extract_dir(self, dirname, path=None):
        dirname = dirname.replace('\\', '/')
        dirname.strip('/')
        dirname += '/'
        names = []
        for name in self.namelist():
            if name.startswith(dirname):
                names.append(name)
        self.extractall(path=path, members=names)

    def __del__(self):
        if not self._closed:
            self.close()


class TaskInfoField(object):
    def __init__(self, key, default=None):
        self._key = key
        self._default = default

    def __get__(self, instance, cls):
        if instance is not None:
            try:
                return instance[self._key]
            except KeyError:
                if self._default is None:
                    raise
                else:
                    return self._default
        return self

    def __set__(self, instance, value):
        instance[self._key] = value


class TaskInfoBase(OrderedDict):
    def __new__(cls, *args, **kwargs):
        obj = super(TaskInfoBase, cls).__new__(cls, *args, **kwargs)
        obj._fields = OrderedDict()
        for name in dir(cls):
            member = getattr(cls, name)
            if isinstance(member, TaskInfoField):
                obj._fields[name] = member
        return obj

    def loads(self, data):
        od = json.loads(data, object_pairs_hook=OrderedDict)
        for name, field in self._fields.items():
            setattr(self, name, od.get(field._key, field._default))
        self.update(od)

    def dumps(self):
        od = OrderedDict()
        for name, field in self._fields.items():
            od[field._key] = getattr(self, name)
        od.update(self)
        return json.dumps(od, indent=2)

    @property
    def fields(self):
        return self._fields


class TaskInfo(TaskInfoBase):
    company = TaskInfoField('company', '')
    project = TaskInfoField('project', '')
    entity_type = TaskInfoField('entity_type', '')
    entity_name = TaskInfoField('entity_name', '')
    task_list = TaskInfoField('task_list', '')


class AssetTaskInfo(TaskInfo):
    asset_category = TaskInfoField('asset_category', 'character')
    asset_parent = TaskInfoField('asset_parent', '')
    asset_type = TaskInfoField('asset_type', '')


class TaskArchive(NebulaArchive):
    _task_filename = 'task_info.json'
    __NaCl = 'taskarchive.nebulapipeline.com'
    _task_info_type = TaskInfo

    def __init__(self, path,
                 mode='r', compression=zipfile.ZIP_DEFLATED, allowZip64=True,
                 pwd=None, task_info=None):
        super(TaskArchive, self).__init__(
                path, mode=mode, compression=compression,
                allowZip64=allowZip64, pwd=pwd)

        self.__task_info = None
        self.__task_info_read = False

        if self.mode == 'w':
            if task_info is None:
                self.__task_info = self._task_info_type()
            if task_info is not None:
                assert isinstance(task_info, self._task_info_type), \
                        "taskinfo must be of type %r" % self._task_info_type
            self.__task_info = task_info

    @property
    def task_info(self):
        if self.__task_info is None:
            if self.mode in 'ra':
                if not self.__task_info_read:
                    data = self.read(self._task_filename)
                    self.__task_info = self._task_info_type()
                    self.__task_info.loads(data)
                    self.__task_info_read = True
            else:
                self.__task_info = self._task_info_type()
        return self.__task_info

    def _write_task_info(self):
        logger.info('Writing task info ...')
        self.writestr(
                self._task_filename,
                self.task_info.dumps())

    def has_submission(self):
        return 'submission/' in self.namelist()

    def close(self):
        if self.mode in 'wa':
            self._write_task_info()
        super(TaskArchive, self).close()


class AssetTaskArchive(TaskArchive):
    _task_info_type = AssetTaskInfo
    _asset_re = re.compile('^[^/]*/$')
    _asset_context_exp = '^%(asset)s(.*)/$'

    def suggest_arcdir(self, assetname, context):
        return '/'.join([assetname, context.replace('\\', '/')])

    def suggest_basename(self, asset_name, context, basename):
        basename, ext = os.path.splitext(basename)
        basename = '%s_%s' % (asset_name, context)
        basename = basename.replace('/', '_').replace('\\', '_')
        basename += ext
        return basename

    def suggest_arcname(self, asset_name, context, filename, rename=True):
        basename = os.path.basename(filename)
        arcfolder = self.suggest_arcdir(asset_name, context)
        if rename:
            basename = self.suggest_basename(asset_name, context, basename)
        return '/'.join([arcfolder, basename])

    def add_asset_file(self, filename,
                       asset_name, context, rename=False):
        arcname = self.suggest_arcname(asset_name, context, filename, rename)
        self.write_empty_dir(os.path.dirname(arcname))
        self.write(filename, arcname=arcname)

    def write_asset_file(
            self, asset_name, context, filename, data, rename=False):
        arcname = self.suggest_arcname(asset_name, context, filename, rename)
        self.write_empty_dir(os.path.dirname(arcname))
        with self.open(arcname, 'w') as _file:
            _file.write(data)

    def list_assets(self):
        assets = []
        for name in self.namelist():
            if name == 'submission/':
                continue
            if self._asset_re.match(name):
                assets.append(name.strip('/'))
        return assets

    def list_asset_contexts(self, asset):
        if not asset.endswith('/'):
            asset += '/'
        self.getinfo(asset)
        ac_re = re.compile(self._asset_context_exp % {'asset': asset})
        contexts = []
        for name in self.namelist():
            match = ac_re.match(name)
            if match:
                contexts.append(match.group(1))
        return contexts

    def list_context_files(self, asset, context):
        context_files = []
        if not asset.endswith('/'):
            asset += '/'
        context = context.replace('\\', '/').strip('/')
        context += '/'
        file_re = re.compile('^%s%s[^/]+$' % (asset, context))
        for name in self.namelist():
            if file_re.match(name):
                context_files.append(name)
        return context_files
