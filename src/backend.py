'''Derive from _DeferredBackend to defer querying backend to when it is
actually needed
'''
import nebula.common.core as core


_unassigned = object()


class _DeferredBackend(object):
    _backend = _unassigned
    _project = ''
    _template = None

    def reset(self):
        pass

    @property
    def backend(self):
        if self._backend is _unassigned:
            self._backend = core.BackendRegistry.get()
        return self._backend

    @property
    def template(self):
        if self._template is None:
            project = self.backend.Project.get(
                    filters=[('code', self._project)])
            if not project:
                self._template = ''
            else:
                self._template = project.template
        return self._template

    def setProject(self, project):
        self._project = project
        self._template = None
        self.reset()

    def getProject(self):
        return self.backend.Project.get(filters=[('code', self._project)])

    def isEpisodic(self):
        return self.template == self.backend.Project.TEMPLATE_EPISODIC
