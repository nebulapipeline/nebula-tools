import subprocess
import tempfile
import os
import shutil


mel_code = """
python("import site; site.addsitedir(r'%(ppath)s'); import %(module)s");
"""

_unknown = object()

class Batchmode(object):
    _file_name = 'batchmode_script'
    
    def __init__(self, appPath, filePath, codePath):
        self._appPath = appPath
        self._filePath = filePath
        self._codePath = codePath
        self._tempdir = tempfile.mkdtemp("", self._file_name)
        self._scriptPath = os.path.join(
            self._tempdir, self._file_name + '.mel')
        self._modulePath = os.path.join(
            self._tempdir, self._file_name + '.py')
    
    
    def setApplicationPath(self, path):
        self._appPath = path
    
    def setFilePath(self, path):
        self._filePath = path
    
    def setCodePath(self, path):
        self._codePath = path
    
    def execute(self):
        commands = self._generate_command()
        self._process = subprocess.Popen(commands, stdout=subprocess.PIPE)
        line = self._process.stdout.readline()
        while line:
            print line
            line = self._process.stdout.readline()    
            
    def _generate_script(self):
        shutil.copy(self._codePath, self._modulePath)
        with open(self._scriptPath, 'w') as _file:
            _file.write(mel_code % {
                'ppath': self._tempdir.replace('\\', '/'),
                'module': self._file_name})
    
    def _generate_command(self):
        commands = []
        commands.append(self._appPath)
        commands.append('-file')
        commands.append(self._filePath)
        commands.append('-script')
        self._generate_script()
        commands.append(self._scriptPath)
        
        return commands
    
if __name__ == '__main__':
    bm = Batchmode(r"c:\Program Files\Autodesk\Maya2018\bin\mayabatch.exe",
              "e:/temp/testme.ma", "e:/temp/testme.py")
    bm.execute()