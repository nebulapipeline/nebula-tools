from nebula.applications import imaya as mi


def check_validity(asset_name, path=None, ref=None):
    '''Check Validity in the current scene, reference or a given maya scene
    If no path or ref is provided the current scene is checked'''
    try:
        if path is not None:
            ref = mi.createReference(path)

        if ref is None:
            geo_sets = mi.get_geo_sets(
                    non_referenced_only=True, valid_only=False)
        else:
            geo_sets = mi.get_geo_sets_from_reference(ref, valid_only=False)

        required_name = "%s_geo_set" % asset_name

        name_found = any((
            mi.getNiceName(_set.name()) == required_name
            for _set in geo_sets))
    finally:
        if ref:
            mi.removeReference(ref)

    return name_found


def check_compatibility(
        path1=None, ref1=None,
        path2=None, ref2=None,
        feedback=False):
    """Check if two scenes or refs are cache compatible. Either of ref1 or
    path1 must be provided.  If neither ref2 or path2 are provided the current
    scene is compared to the content of path1 or ref1"""

    result = False
    try:
        if path1 is not None:
            ref1 = mi.createReference(path1)
        if path2 is not None:
            ref2 = mi.createReference(path2)

        if not ref1:
            raise RuntimeError('Must provide either ref1 or path1')

        result = mi.refs_compatible(ref1, ref2, feedback=feedback)

    finally:
        if ref1:
            mi.removeReference(ref1)
            mi.removeReference(ref2)

    return result
