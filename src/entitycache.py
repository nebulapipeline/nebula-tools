''' Contains a descriptor called EntityCache which lazily queries the backend
when read
'''

import logging
from .backend import _DeferredBackend
from nebula.common.util import InsertableOrderedDict


_unassigned = object()
logger = logging.getLogger(__name__)


class EntityCache(_DeferredBackend):
    '''Descriptor to hold queried entity list for a specific type'''

    def __init__(self, attr, populator=None, entity=None, entity_type=None,
                 filters=None, order_bys=None):
        self.attr = attr
        self.populator = populator
        self._entity = entity
        self.entity_type = entity_type
        self.filters = filters
        self.order_bys = order_bys
        self._obj = _unassigned

    @property
    def entity(self):
        if not self._entity:
            self._entity = self.backend._getClass(
                    self.entity_type.__entity_type__)
        return self._entity

    @property
    def project(self):
        if hasattr(self._obj, '_project'):
            return self._obj._project

    def populate(self):
        project = self.project
        if project is not None:
            self.entity.setProject(project)

        data = InsertableOrderedDict()
        try:
            if self.populator:
                data = self.populator()
            elif self.entity or self.entity_type:
                data = self.entity.filter(
                        filters=self.filters, order_bys=self.order_bys)
        except Exception as exc:
            logger.error(str(exc))
        finally:
            self.set(data)
        return getattr(self._obj, self.attr)

    def __delete__(self, obj):
        if obj is None:
            return
        self._obj = obj
        if hasattr(obj, self.attr):
            delattr(obj, self.attr)

    def __get__(self, obj, objtype=None):
        if obj is None:
            return self
        self._obj = obj
        value = getattr(obj, self.attr, _unassigned)
        if value is _unassigned:
            value = self.populate()
        return value

    def __set__(self, obj, value):
        if obj is None:
            return
        self._obj = obj
        self.set(value)

    def set(self, value):
        if value is None:
            setattr(self._obj, self.attr, _unassigned)
            return
        if isinstance(value, dict):
            if not isinstance(value, InsertableOrderedDict):
                value = InsertableOrderedDict(value)
            setattr(self._obj, self.attr, value)
            return
        setattr(self._obj, self.attr,
                InsertableOrderedDict(((
                    entity.name, entity) for entity in value)))
