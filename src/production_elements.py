import os
import re
import string
from collections import defaultdict, namedtuple


from nebula.common import core
from .entitycache import EntityCache
from .backend import _DeferredBackend


class NameBuilder(
        namedtuple('_NameBuilder', ['kw', 'pad', 'sc', 're'])):

    def build_re(self):
        return self.re.format(kw=self.kw, pad=self.pad)

    def build_short_name(self, num, letter):
        return self.sc.format(kw=self.kw, pad=self.pad, num=num, letter=letter)


def _hastype(one, two):
    return isinstance(one, two) or one == two


class Conventions(_DeferredBackend):
    _default_sc = '{kw:.3s}{num:0{pad:d}d}{letter:.1s}'
    _default_re = r'{kw:.3s}(\d{{{pad},}})([a-z]?)'

    ep = NameBuilder('EP', 2, _default_sc, _default_re)
    sq = NameBuilder('SQ', 3, _default_sc, _default_re)
    sh = NameBuilder('SH', 3, _default_sc, _default_re)

    sort_order_mult = 10
    shot_length = 100
    handle_in = 0
    handle_out = 0

    episodes = EntityCache(
            '_episodes', entity_type=core.IEpisode, order_bys=['sort_order'])
    sequences = EntityCache(
            '_sequences', entity_type=core.ISequence, order_bys=['sort_order'])
    shots = EntityCache(
            '_shots', entity_type=core.IShot, order_bys=['sort_order'])

    def __init__(self, episodes=None, sequences=None, shots=None,
                 project=None):
        self.episodes = episodes
        self.sequences = sequences
        self.shots = shots
        self._project = project

    def reset(self):
        self.episodes = None
        self.sequences = None
        self.shots = None

    @classmethod
    def get_cb(cls, typ):
        if _hastype(typ, core.IShot):
            return cls.sh
        elif _hastype(typ, core.ISequence):
            return cls.sq
        elif _hastype(typ, core.IEpisode):
            return cls.ep

    @classmethod
    def get_re(cls, typ):
        return cls.get_cb(typ).build_re()

    @classmethod
    def get_short_name(cls, typ, num, letter):
        return cls.get_cb(typ).build_short_name(num, letter)

    def get_num_and_letter(self, objs, func=None):
        if func is None:
            func = min
        num, letters = None, ''
        num_to_letters = defaultdict(list)

        for obj in objs:
            match = re.search(self.get_cb(obj).build_re(), obj.name, re.I)
            if match:
                _num = int(match.group(1))
                if match.group(2):
                    num_to_letters[_num].append(match.group(2))
                if num is None:
                    num = _num
                num = func(num, _num)

        letters = set(num_to_letters[num])

        for letter in string.letters[:26]:
            if letter not in letters:
                break
        else:
            letter = ''

        return num, letter

    @classmethod
    def get_parent_attr(cls, typ):
        if _hastype(typ, core.ISequence):
            return 'episode'
        elif _hastype(typ, core.IShot):
            return 'sequence'
        return ''

    @classmethod
    def get_parent_type(cls, typ):
        if _hastype(typ, core.ISequence):
            return core.IEpisode
        elif _hastype(typ, core.IShot):
            return core.ISequence

    @classmethod
    def entity_has_parent(self, entity, parent):
        parent_attr = self.get_parent_attr(entity)
        if parent_attr:
            return getattr(entity, parent_attr) == parent
        return True

    def parent_exists(self, typ, parent=''):
        parent_type = self.get_parent_type(typ)
        if parent_type:
            if parent not in self.get_entity_cache(parent_type):
                return False
        return True

    def get_entity_cache(self, typ):
        if _hastype(typ, core.IEpisode):
            return self.episodes
        elif _hastype(typ, core.ISequence):
            return self.sequences
        elif _hastype(typ, core.IShot):
            return self.shots

    def get_overlapping(self, typ,
                        sort_order=None, parent=''):
        if sort_order is None:
            sort_order = self.suggest_sort_order(typ)

        return [entity
                for entity in self.get_entity_cache(typ).values()
                if self.entity_has_parent(entity, parent) and
                entity.sort_order == sort_order]

    def get_preceding(self, typ, sort_order=None, parent='', limit=False):
        result = []
        if sort_order is None:
            sort_order = self.suggest_sort_order()

        parent_attr = self.get_parent_attr(typ)
        result = [entity
                  for entity in self.get_entity_cache(typ).values()
                  if self.entity_has_parent(entity, parent=parent) and
                  entity.sort_order < sort_order]
        result = sorted(result, key=lambda e: e.sort_order, reverse=True)

        if not limit and parent_attr:
            parent_type = self.get_parent_type(typ)
            ppattr = self.get_parent_attr(parent_type)
            parent_cache = self.get_entity_cache(parent_type)
            pobj = parent_cache[parent]

            while not result:
                prec = self.get_preceding(
                        typ=parent_type,
                        sort_order=pobj.sort_order,
                        parent=getattr(pobj, ppattr) if ppattr else '')

                if not prec:
                    break
                else:
                    prec = filter(
                            lambda x: x.sort_order == prec[0].sort_order,
                            prec)
                    result = filter(
                            lambda e: getattr(e, parent_attr) in [
                                p.name for p in prec],
                            self.get_entity_cache(typ).values())
                    pobj = prec[0]

        return result

    def get_succeeding(self, typ, sort_order=None, parent='', limit=True):
        result = []
        if sort_order is None:
            sort_order = self.suggest_sort_order()

        parent_attr = self.get_parent_attr(typ)
        result = [entity
                  for entity in self.get_entity_cache(typ).values()
                  if self.entity_has_parent(entity, parent=parent) and
                  entity.sort_order > sort_order]
        result = sorted(result, key=lambda e: e.sort_order)

        if not limit and parent_attr:
            parent_type = self.get_parent_type(typ)
            ppattr = self.get_parent_attr(parent_type)
            parent_cache = self.get_entity_cache(parent_type)
            pobj = parent_cache[parent]

            while not result:
                prec = self.get_succeeding(
                        typ=parent_type,
                        sort_order=pobj.sort_order,
                        parent=getattr(pobj, ppattr) if ppattr else '')

                if not prec:
                    break
                else:
                    prec = filter(
                            lambda x: x.sort_order == prec[0].sort_order,
                            prec)
                    result = filter(
                            lambda e: getattr(e, parent_attr) in [
                                p.name for p in prec],
                            self.get_entity_cache(typ).values())
                    pobj = prec[0]

        return result

    def suggest_sort_order(self, typ, parent=''):
        if not self.parent_exists(typ, parent):
            raise ValueError('The entity "%s" does not exist' % parent)
        sort_order = self.sort_order_mult
        sort_orders = [
            entity.sort_order or 0
            for entity in self.get_entity_cache(typ).values()
            if self.entity_has_parent(entity, parent)]
        if sort_orders:
            sort_order = max(sort_orders) + self.sort_order_mult
            sort_order /= float(self.sort_order_mult)
            sort_order = int(round(sort_order)) * self.sort_order_mult
        return sort_order

    def suggest_short_name(self, typ, sort_order=None, parent=''):
        if not self.parent_exists(typ, parent):
            raise ValueError('The entity "%s" does not exist' % parent)

        if sort_order is None:
            sort_order = self.suggest_sort_order(typ, parent)

        num, letter = 1, ''
        overlaps = self.get_overlapping(typ, sort_order, parent=parent)

        if overlaps:
            num, letter = self.get_num_and_letter(overlaps)
        else:
            preceding = self.get_preceding(
                    typ, sort_order, parent=parent, limit=True)
            succeeding = self.get_succeeding(
                    typ, sort_order, parent=parent, limit=True)

            pre_num, pre_letter = (self.get_num_and_letter(preceding, max)
                                   if preceding else (None, None))
            suc_num, suc_letter = (self.get_num_and_letter(succeeding, min)
                                   if succeeding else (None, None))

            if pre_num is not None:
                num = pre_num + 1
                if suc_num is not None and num == suc_num:
                    num, letter = pre_num, pre_letter
                else:
                    if suc_num is not None:
                        if suc_num > 1:
                            num = suc_num - 1
                        else:
                            num = suc_num
                            letter = suc_letter

        return self.get_short_name(typ, num, letter)

    def suggest_name(self, typ, sort_order=None, parent=''):
        if sort_order is None:
            sort_order = self.suggest_sort_order(typ, parent)
        short_name = self.suggest_short_name(typ, sort_order, parent=parent)
        if _hastype(typ, core.ISequence):
            if self.isEpisodic() and parent:
                return '_'.join([parent, short_name])
            return short_name
        elif _hastype(typ, core.IShot):
            return '_'.join([parent, short_name])
        elif _hastype(typ, core.IEpisode):
            return short_name

    def suggest_frame_range(self, sequence, sort_order=None):
        typ = core.IShot
        if not self.parent_exists(typ, sequence):
            raise ValueError('The entity "%s" does not exist' % sequence)

        if sort_order is None:
            sort_order = self.suggest_sort_order(typ, parent=sequence)

        assert(sequence in self.sequences)

        start_frame, frame_in, frame_out, end_frame = None, 0, 0, None
        overlapping_shots = self.get_overlapping(typ, sort_order, sequence)

        if overlapping_shots:
            start_frame = min((shot.start_frame for shot in overlapping_shots))
            frame_in = min((shot.frame_in for shot in overlapping_shots))
            frame_out = min((shot.frame_out for shot in overlapping_shots))
            end_frame = min((shot.end_frame for shot in overlapping_shots))

        else:
            preceding_shots = self.get_preceding(typ, sort_order, sequence)
            succeeding_shots = self.get_succeeding(typ, sort_order, sequence)

            if preceding_shots:
                frame_in = max((
                    shot.frame_out for shot in preceding_shots)) + 1

            if succeeding_shots:
                frame_out = min((
                    shot.frame_in for shot in succeeding_shots)) - 1
            else:
                frame_out = frame_in + self.shot_length - 1

        if start_frame is None:
            start_frame = frame_in - self.handle_in
        if end_frame is None:
            end_frame = frame_out + self.handle_out

        return (start_frame, frame_in, frame_out, end_frame)

    def create_new(self, typ, parent='', description=''):
        cls = self.backend._getClass(typ.__entity_type__)
        elem = cls()
        elem.sort_order = self.suggest_sort_order(typ, parent=parent)
        elem.name = self.suggest_name(
                typ, sort_order=elem.sort_order, parent=parent)
        elem.description = description
        parent_attr = self.get_parent_attr(typ)

        if parent_attr:
            if parent not in self.get_entity_cache(self.get_parent_type(typ)):
                raise ValueError("parent entity '%s' does not exist")
            setattr(elem, parent_attr, parent)

        if _hastype(typ, core.IShot):
            (elem.start_frame, elem.frame_in, elem.frame_out,
             elem.end_frame) = self.suggest_frame_range(
                            parent, sort_order=elem.sort_order)
        return elem


def by_sort_order(iodict, key, value, ref_key):
    return value.sort_order < iodict[ref_key].sort_order


class Manager(_DeferredBackend):
    episodes = EntityCache(
            '_episodes', entity_type=core.IEpisode, order_bys=['sort_order'])
    sequences = EntityCache(
            '_sequences', entity_type=core.ISequence, order_bys=['sort_order'])
    shots = EntityCache(
            '_shots', entity_type=core.IShot, order_bys=['sort_order'])

    def __init__(self, project='', episodes=None, sequences=None, shots=None):
        self.setProject(project)
        if episodes is not None:
            self.episodes = episodes
        if sequences is not None:
            self.sequences = sequences
        if shots is not None:
            self.shots = shots
        self._marked_eps = set()
        self._marked_seqs = set()
        self._marked_shots = set()
        self.issues = {}

    @property
    def conv(self):
        return Conventions(
                episodes=self.episodes, sequences=self.sequences,
                shots=self.shots, project=self._project)

    def reset(self):
        del self.episodes
        del self.sequences
        del self.shots
        self._marked_eps = set()
        self._marked_seqs = set()
        self._marked_shots = set()
        self.issues = {}

    def episode_sequences(self, episode):
        if isinstance(episode, core.IEpisode):
            ep_seqs = [seq
                       for seq in self.sequences.values()
                       if seq.episode == episode.name]
            return ep_seqs

    def sequence_shots(self, sequence):
        if isinstance(sequence, core.ISequence):
            seq_shots = [shot
                         for shot in self.shots.values()
                         if shot.sequence == sequence.name]
            return seq_shots
        return []

    def episode_shots(self, episode):
        if isinstance(episode, core.IEpisode):
            shots = []
            for seq in self.episode_sequences(episode):
                shots.extend(self.sequence_shots(seq))
            return shots
        return []

    def get_parent(self, elem):
        if elem is not None and not isinstance(elem, core.IProductionElement):
            raise TypeError(
                    "Element provided must be a production element or None")
        if isinstance(elem, core.IShot):
            return self.sequences.get(elem.sequence)
        elif isinstance(elem, core.ISequence) and self.isEpisodic():
            return self.episodes.get(elem.episode)

    def get_siblings(self, elem=None):
        if not isinstance(elem, core.IProductionElement):
            raise TypeError(
                    "Element provided must be a production element")
        siblings = []
        if isinstance(elem, core.IShot):
            parent = self.get_parent(elem)
            siblings = self.sequence_shots(parent)
        elif isinstance(elem, core.ISequence):
            parent = self.get_parent(elem)
            siblings = self.episode_sequences(parent)
        elif isinstance(elem, core.IEpisode):
            siblings = self.episodes.values()
        return siblings

    def get_children(self, elem=None):
        if isinstance(elem, core.IEpisode):
            return self.episode_sequences(elem)
        elif isinstance(elem, core.ISequence):
            return self.sequence_shots(elem)
        elif elem is None:
            if self.isEpisodic():
                return self.episodes.values()
            return self.sequences.values()
        elif isinstance(elem, core.IShot):
            return []
        raise TypeError("Element must be a production element")

    def get_overlapping(self, elem):
        overlapping = []
        for sibling in self.get_siblings(elem):
            if sibling.sort_order == elem.sort_order:
                overlapping.append(sibling)
        return overlapping

    def _get_preceding(self, elem):
        siblings = self.get_siblings(elem)
        filtered = sorted(
                filter(
                    lambda elem1: elem1.sort_order < elem.sort_order,
                    siblings),
                key=lambda el: el.sort_order)
        if filtered:
            filtered = [el for el in filtered
                        if el.sort_order == filtered[-1].sort_order]
        return filtered

    def _get_succeeding(self, elem):
        siblings = self.get_siblings(elem)
        filtered = sorted(
                filter(
                    lambda elem1: elem1.sort_order > elem.sort_order,
                    siblings),
                key=lambda el: el.sort_order)
        if filtered:
            filtered = [el for el in filtered
                        if el.sort_order == filtered[0].sort_order]
        return filtered

    def get_preceding(self, elem):
        preceding = self._get_preceding(elem)

        # special case for shots if there is no preceding shots
        if isinstance(elem, core.IShot) and not preceding:
            sequence = self.sequences[elem.sequence]
            while not preceding:
                preceding_seqs = self._get_preceding(sequence)

                # if there is not preceding sequences just return empty
                if not preceding_seqs:
                    break

                # get last shots of preceding sequence
                for sequence in preceding_seqs:
                    seq_shots = sorted(
                            self.sequence_shots(sequence),
                            key=lambda el: el.sort_order)
                    if not seq_shots:
                        continue
                    preceding.extend([
                        shot for shot in seq_shots
                        if shot.sort_order == seq_shots[-1].sort_order])

        return preceding

    def get_succeeding(self, elem):
        preceding = self._get_succeeding(elem)

        # special case for shots if there is no preceding shots
        if isinstance(elem, core.IShot) and not preceding:
            sequence = self.sequences[elem.sequence]
            while not preceding:
                preceding_seqs = self._get_succeeding(sequence)

                # if there is not preceding sequences just return empty
                if not preceding_seqs:
                    break

                # get last shots of preceding sequence
                for sequence in preceding_seqs:
                    seq_shots = sorted(
                            self.sequence_shots(sequence),
                            key=lambda el: el.sort_order)
                    if not seq_shots:
                        continue
                    preceding.extend([
                        shot for shot in seq_shots
                        if shot.sort_order == seq_shots[0].sort_order])

        return preceding

    def status(self, elem):
        if isinstance(elem, core.IShot):
            shot = elem
            if shot.frame_out < shot.frame_in:
                return 'Negative Range'
            if shot.start_frame > shot.frame_in:
                return 'Negative In Handle'
            if shot.end_frame < shot.frame_out:
                return 'Negative Out Handle'
            for preceding in self.get_preceding(elem):
                if preceding.frame_out > shot.frame_in:
                    return ('Overlapping with previous Shot "%s"' %
                            preceding.name)
                if preceding.end_frame - preceding.start_frame > 1:
                    return ('%d frames missing between %s and %s' % (
                            preceding.name, shot.name))
        return 'OK'

    def create_new(self, typ, name=None, sort_order=None, description=None):
        cls = self.backend._getClass(typ.__entity_type__)
        obj = cls()

        if name is not None:
            obj.name = name
        if sort_order is not None:
            obj.sort_order = sort_order
        if description is not None:
            obj.description = description

        return obj

    def add_element(self, elem, save=False):
        if isinstance(elem, core.IShot):
            if not self.sequences.get(elem.sequence):
                raise ValueError("Sequence %s does not exist" % elem.sequence)
            self.shots[elem.name] = elem
        elif isinstance(elem, core.ISequence):
            if not self.episodes.get(elem.episode):
                raise ValueError("Episode %s does not exist" % elem.episode)
            self.sequences[elem.name] = elem
        elif isinstance(elem, core.IEpisode):
            self.episodes[elem.name] = elem
        if save:
            elem.save()
        return elem

    def remove_element(self, elem, remove=True):
        name = elem.name
        if isinstance(elem, core.IShot):
            del self.shots[name]
        elif isinstance(elem, core.ISequence):
            del self.sequences[name]
        elif isinstance(elem, core.IEpisode):
            del self.episodes[name]
        if remove:
            elem.remove()

    def update_element(self, incoming, save=False):
        parent_cache = cache = None
        _type = 'Element'
        parent = ''

        if isinstance(incoming, core.IShot):
            cache = self.shots
            parent_cache = self.sequences
            _type = core.IShot
            parent = 'sequence'

        elif isinstance(incoming, core.ISequence):
            cache = self.sequences
            parent_cache = self.episodes
            _type = core.ISequence
            parent = 'episode'

        elif isinstance(incoming, core.IEpisode):
            cache = self.episodes
            _type = core.IEpisode

        existing = cache.get(incoming.name)
        if not existing:
            raise ValueError(
                    'Cannot Update non-existant %s: %s'
                    % (_type, incoming.name))
        if parent:
            parent_name = getattr(incoming, parent)
            parent = parent_cache.get(parent_name)
        existing.update(incoming)

        if save:
            existing.save()

    def element_exists(self, elem):
        if isinstance(elem, core.IEpisode):
            return elem.name in self.episodes
        elif isinstance(elem, core.ISequence):
            return elem.name in self.sequences
        elif isinstance(elem, core.IShot):
            return elem.name in self.shots
        raise TypeError('Unknown object of type (%r) passed' % type(elem))

    def saveEpisodes(self):
        self.episodes = self.backend.Episode.saveMultiple(
                self.episodes.values())

    def saveSequences(self):
        self.sequences = self.backend.Sequence.saveMultiple(
                self.sequences.values())

    def saveShots(self):
        self.shots = self.backend.Shot.saveMultiple(self.shots.values())

    def saveAll(self):
        self.saveEpisodes()
        self.saveSequences()
        self.saveShots()

    def update_ep_from_path(self, ep, path):
        episode = self.episodes[ep]
        reader = EpisodePathReader(ep, path, self._project)
        reader.populate()

        for sequence in self.episode_sequences(episode):
            if sequence.name in reader.sequences:
                sequence.update(reader.sequences[sequence.name])
            else:
                self.mark(sequence)

        for shot in self.episode_shots(episode):
            if shot.name in reader.shots:
                shot.update(reader.shots[shot.name])
            else:
                self.mark(shot)

        for sequence in reader.sequences.values():
            if sequence.name not in self.sequences:
                self.sequences[sequence.name] = sequence

        for shot in reader.shots.values():
            if shot.name not in self.shots:
                if not hasattr(shot, 'frame_in'):
                    (shot.start_frame, shot.frame_in, shot.frame_out,
                     shot.end_frame) = self.conv.suggest_frame_range(
                             shot.sequence, shot.sort_order)
                self.shots[shot.name] = shot

    def removeMarked(self):
        if self._marked_shots:
            self.backend.Shot.removeMultiple(
                    [self.shots[elem] for elem in self._marked_shots
                     if self.shots[elem].exists()])
            for elem in self._marked_shots:
                del self.shots[elem]
            self._marked_shots = set()

        if self._marked_seqs:
            self.backend.Sequence.removeMultiple(
                    [self.sequences[elem] for elem in self._marked_seqs
                     if self.sequences[elem].exists()])
            for elem in self._marked_seqs:
                del self.sequences[elem]
            self._marked_seqs = set()

        if self._marked_eps:
            self.backend.Episode.removeMultiple(
                    [self.episodes[elem] for elem in self._marked_eps
                     if self.episodes[elem].exists()])
            for elem in self._marked_eps:
                del self.episodes[elem]
            self._marked_eps = set()

    def mark(self, elem):
        if isinstance(elem, core.IProductionElement):
            elem = elem.name

        if elem in self.episodes:
            self._marked_eps.add(elem)
            return True
        elif elem in self.sequences:
            self._marked_seqs.add(elem)
            return True
        elif elem in self.shots:
            self._marked_shots.add(elem)
            return True
        return False

    def unmark(self, elem):
        if isinstance(elem, core.IProductionElement):
            elem = elem.name

        if elem in self._marked_eps:
            self._marked_eps.remove(elem)
            return True
        if elem in self._marked_seqs:
            self._marked_seqs.remove(elem)
            return True
        if elem in self._marked_shots:
            self._marked_shots.remove(elem)
            return True
        return False

    def syncUp(self):
        self.saveAll()
        self.deleteMarked()

    def isMarked(self, elem):
        name = elem
        if isinstance(elem, core.IProductionElement):
            name = elem.name
        if name in self.sequences:
            if name in self._marked_seqs:
                return True
            return False
        if name in self.shots:
            if name in self._marked_shots:
                return True
            return False
        if name in self.episodes:
            if name in self._marked_eps:
                return True
            return False
        raise ValueError('Element "%s" does not exist' % elem)


class EpisodePathReader(_DeferredBackend):
    seq_re = re.compile(Conventions.sq.build_re(), re.I)
    shots_re = re.compile(Conventions.sh.build_re(), re.I)
    fnum_re = re.compile(r'^.*\.(-?\d+)\.[^\.]*$')
    seq_folder = 'SEQUENCES'
    shot_folder = 'SHOTS'

    episodes = EntityCache('_episodes', list, entity_type=core.IEpisode)
    sequences = EntityCache('_sequences', list, entity_type=core.ISequence)
    shots = EntityCache('_shots', list, entity_type=core.IShot)

    def __init__(self, ep, path, project=None):
        self.ep = ep
        self.path = path
        self._project = project

    def reset(self):
        self.episodes = None
        self.sequences = None
        self.shots = None

    def get_sequences_dir(self):
        return os.path.join(self.path, self.seq_folder)

    def get_shots_dir(self, seq):
        seqs_dir = self.get_sequences_dir()
        return os.path.join(seqs_dir, seq, self.shot_folder)

    def read_sequences(self):
        seqs_dir = self.get_sequences_dir()

        if not os.path.isdir(seqs_dir):
            raise IOError("Cannot find the '%s' directory" % self.seq_folder)

        seqs = []
        for seq in os.listdir(seqs_dir):
            match = self.seq_re.match(seq)
            if match:
                seqs.append((seq, int(match.group(1)), match.group(2)))
        sorted(seqs, key=lambda seq: seq[1])

        return seqs

    def read_shots(self, seq):
        shots_dir = self.get_shots_dir(seq)

        if not os.path.isdir(shots_dir):
            raise IOError("Cannot find the '%s' directory for seq '%s'" % (
                self.shot_folder, seq))

        shots_info = []
        for shot in os.listdir(shots_dir):
            match = self.shots_re.search(shot)
            if match:
                shots_info.append((
                    shot, seq, int(match.group(1)), match.group(2),
                    self.get_shot_frame_range(os.path.join(shots_dir, shot))))

        shots_info = sorted(shots_info, key=lambda shot: shot[2])
        return shots_info

    def get_shot_frame_range(self, shot_path):
        frange = (None, None)
        animatic_path = os.path.join(shot_path, 'animatic')
        if os.path.exists(animatic_path):
            files = os.listdir(animatic_path)
            nums = []
            for _file in files:
                match = self.fnum_re.match(_file)
                if match:
                    num = int(match.group(1))
                    nums.append(num)
            if nums:
                frange = (min(nums), max(nums))
        return frange

    def read(self):
        if not os.path.isdir(self.path):
            raise IOError("Cannot find the directory '%s'" % self.path)

        seqs = self.read_sequences()
        shots = []
        for seq in seqs:
            shots.extend(self.read_shots(seq[0]))
        return seqs, shots

    def populate(self):
        seqs, shots_info = self.read()

        ep = self.backend.Episode()
        ep.name = self.ep
        self.episodes[self.ep] = ep

        for seq, num, letter in seqs:
            name = '_'.join([
                self.ep, Conventions.sq.build_short_name(num, letter)])
            sobj = self.backend.Sequence()
            sobj.name = name
            sobj.episode = self.ep
            sobj.sort_order = num * Conventions.sort_order_mult
            self.sequences[name] = sobj

        for shot, seq, num, letter, frange in shots_info:
            name = '_'.join([
                self.ep, seq, Conventions.sh.build_short_name(num, letter)])
            sobj = self.backend.Shot()
            sobj.name = name
            sobj.sequence = '_'.join([self.ep, seq])
            sobj.sort_order = num * Conventions.sort_order_mult
            if None not in frange:
                sobj.start_frame = sobj.frame_in = frange[0]
                sobj.end_frame = sobj.frame_out = frange[1]
            self.shots[name] = sobj
