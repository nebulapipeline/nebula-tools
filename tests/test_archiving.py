'''
test_archiving.py

Testing function of archiving utility
'''


import unittest
import tempfile
import os
import shutil
import HTMLParser
import zipfile
import random
import string
import urllib2

import nebula.common.tools as tools
import nebula.common.util as util


class RandomTextHTMLParser(HTMLParser.HTMLParser):
    def __init__(self, *args, **kwargs):
        HTMLParser.HTMLParser.__init__(self, *args, **kwargs)
        self._handle_data = False
        self.randomtext = ''

    def handle_starttag(self, tag, attrs):
        attrs = dict(attrs)
        if tag == 'textarea' and\
                attrs.get('id', '') == 'generatedtext':
            self._handle_data = True

    def handle_endtag(self, tag):
        if self._handle_data and tag == 'textarea':
            self._handle_data = False

    def handle_data(self, data):
        if self._handle_data:
            self.randomtext = data


def random_text_from_internet():
    p = RandomTextHTMLParser()
    f = urllib2.urlopen('https://randomtextgenerator.com/')
    p.feed(f.read())
    return p.randomtext


def random_text():
    num = random.randint(50, 100)
    text = ''
    for i in xrange(num):
        text += random.choice(string.ascii_letters + ' ' * 7 + '.,\n')
    return text


class ArchiveTestCase(unittest.TestCase):
    entity_name = 'bilal'
    data = {
                entity_name: {
                    'brief': ['file_%02d.txt' % i for i in range(2)],
                    'artwork': ['image_%02d.jpg' % i for i in range(3)],
                    'artwork/reference': ['ref.jpg']
                    }
           }

    @classmethod
    def write_file(cls, asset, context, filename):
        dirname = os.path.join(cls.tempdir, asset, context)
        util.mkdirr(dirname)
        if os.path.isdir(dirname):
            with open(os.path.join(dirname, filename), 'w') as _file:
                _file.write(random_text())

    @classmethod
    def setUpData(cls):
        cls.tempdir = tempfile.mkdtemp(prefix='nebarch_')
        cls.zip_loc = cls.tempdir + '.zip'
        for asset in cls.data:
            for context in cls.data[asset]:
                for filename in cls.data[asset][context]:
                    cls.write_file(asset, context, filename)

    @classmethod
    def setUpClass(cls):
        cls.setUpData()
        with tools.AssetTaskArchive(cls.tempdir + '.zip', 'w') as arch:
            arch.task_info.entity_name = cls.entity_name
            arch.task_info.task_list = 'M'
            for asset in cls.data:
                arch.write_dir(
                        os.path.join(cls.tempdir, asset), arch_dir=asset)

    @classmethod
    def tearDownClass(cls):
        if os.path.exists(cls.tempdir):
            shutil.rmtree(cls.tempdir)
        if os.path.isfile(cls.zip_loc):
            os.unlink(cls.zip_loc)

    def setUp(self):
        self.arch = tools.AssetTaskArchive(self.zip_loc)

    def tearDown(self):
        self.arch.close()


class TestAssetTaskArchive(ArchiveTestCase):
    '''A class to test'''
    def test_archiving(self):
        self.assertTrue(os.path.isfile(self.zip_loc))

    def test_task_info(self):
        self.assertIn('task_info.json', self.arch.namelist())

    def test_hash_info(self):
        self.assertIn('hash_info.json', self.arch.namelist())

    def test_validate(self):
        self.assertTrue(self.arch.validate())

    def test_files(self):
        for asset in self.data:
            for context in self.data[asset]:
                for filename in self.data[asset][context]:
                    self.assertIn(
                            '/'.join([asset, context, filename]),
                            self.arch.namelist())

    def test_assets(self):
        self.assertSetEqual(
                set(self.arch.list_assets()),
                set([self.entity_name]))

    def test_contexts(self):
        self.assertSetEqual(
                set(self.arch.list_asset_contexts(self.entity_name)),
                set(self.data[self.entity_name].keys()))

    def test_context_files(self):
        for context in self.data[self.entity_name]:
            self.assertSetEqual(
                    set([os.path.basename(p)
                        for p in self.arch.list_context_files(
                            self.entity_name, context)]),
                    set(self.data[self.entity_name][context]))

    def test_task_info_unknown_attr(self):
        with self.assertRaises(AttributeError):
            print self.arch.task_info.unknown_attr


class TestInvalidArchive(ArchiveTestCase):
    _file = 'impostor.txt'

    @classmethod
    def setUpClass(cls):
        super(TestInvalidArchive, cls).setUpClass()
        with zipfile.ZipFile(cls.zip_loc, mode='a') as arch:
            arch.writestr(cls._file, random_text())

    def test_invalidity(self):
        self.assertFalse(self.arch.validate())

    def test_errors(self):
        self.arch.validate()
        self.assertIn(self._file, self.arch.errors)

    def test_check_validate(self):
        with self.assertRaises(tools.src.archiving.FailedIntegrityCheck):
            self.arch.check_validate()

    def test_task_info_unknown_attr(self):
        with self.assertRaises(AttributeError):
            print self.arch.task_info.unknown_attr


class TestUnknownArchive(ArchiveTestCase):

    @classmethod
    def setUpClass(cls):
        cls.setUpData()
        with zipfile.ZipFile(cls.zip_loc, 'w') as arch:
            directory = cls.tempdir
            for dirpath, dirnames, filenames in os.walk(directory):
                for filename in filenames + dirnames:
                    full_path = os.path.join(dirpath, filename)
                    arch_path = os.path.relpath(full_path, directory)
                    arch.write(full_path, arch_path)

    def test_unknown_archive_task_info(self):
        with self.assertRaises(KeyError):
            self.arch.task_info

    def test_unknown_archive_hash_info(self):
        with self.assertRaises(KeyError):
            self.arch.hash_info

    def test_unknown_archive_validate(self):
        with self.assertRaises(KeyError):
            self.arch.check_validate()

        with self.assertRaises(KeyError):
            self.assertFalse(self.arch.validate())


if __name__ == "__main__":
    unittest.main()
