import nebula.common.tools.src.checks as checks
from nebula.applications import imaya as mi

import pymel.core as pc

import tempfile
import unittest
import os


def mayaScene(func):
    def _maya_scene_wrapper(filename):
        pc.newFile(f=True)
        retval = func()
        pc.renameFile(filename)
        typ = 'mayaAscii'
        _, ext = os.path.splitext(filename)
        if ext == ".mb":
            typ = 'mayaBinary'
        pc.saveFile(f=True, type=typ)
        return retval
    return _maya_scene_wrapper


@mayaScene
def create_valid_maya_asset():
    stuff = []
    stuff.append(pc.polySphere(r=1)[0])
    stuff.append(pc.polyCube()[0])
    pc.select(stuff)
    setname = pc.sets()
    pc.rename(setname, 'myasset_geo_set')


@mayaScene
def create_invalid_maya_asset():
    pc.polySphere()


@mayaScene
def create_compatible_scene():
    sphere, _ = pc.polySphere()
    cube, _ = pc.polyCube()
    sphere.rename('head')
    cube.rename('body')
    pc.move(sphere, 1, 0, 0, r=True)
    pc.scale(cube, 2, 0, 2, r=True)
    pc.move(sphere.vtx[0], 1, 0, 0, r=True)
    pc.circle()
    pc.select(sphere, cube)
    _set = pc.sets()
    _set.rename('myasset_geo_set')


@mayaScene
def create_incompatible_scene():
    sphere, _ = pc.polySphere()
    pc.select(sphere)
    _set = pc.sets()
    _set.rename('myasset_get_set')


class TestCheckValidity(unittest.TestCase):
    asset_name = 'myasset'

    @classmethod
    def setUpClass(cls):
        cls.valid_file = tempfile.mktemp(
                suffix=".ma", prefix="valid_maya_asset")
        cls.invalid_file = tempfile.mktemp(
                suffix=".ma", prefix="invalid_maya_asset")
        create_valid_maya_asset(cls.valid_file)
        create_invalid_maya_asset(cls.invalid_file)

    @classmethod
    def tearDownClass(cls):
        if os.path.exists(cls.valid_file):
            os.unlink(cls.valid_file)
        if os.path.exists(cls.invalid_file):
            os.unlink(cls.invalid_file)

    def setUp(self):
        pc.newFile(f=True)

    def tearDown(self):
        pc.newFile(f=True)

    def test_valid_scene_current(self):
        pc.openFile(self.valid_file)
        self.assertTrue(checks.check_validity(self.asset_name))

    def test_valid_scene_ref(self):
        ref = mi.createReference(self.valid_file)
        self.assertTrue(checks.check_validity(self.asset_name, ref=ref))

    def test_valid_scence_path(self):
        self.assertTrue(
                checks.check_validity(self.asset_name, path=self.valid_file))

    def test_invalid_scene_current(self):
        pc.openFile(self.invalid_file)
        self.assertFalse(checks.check_validity(self.asset_name))

    def test_invalid_scene_ref(self):
        ref = mi.createReference(self.invalid_file)
        self.assertFalse(checks.check_validity(self.asset_name, ref=ref))

    def test_invalid_scence_path(self):
        self.assertFalse(
                checks.check_validity(self.asset_name, path=self.invalid_file))


class TestCheckCompatibility(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.valid_path = tempfile.mktemp(
                suffix=".ma", prefix="valid_asset_")
        cls.invalid_path = tempfile.mktemp(
                suffix=".ma", prefix="invalid_asset_")
        cls.compatible_path = tempfile.mktemp(
                suffix=".ma", prefix="compatible_asset_")
        cls.incompatible_path = tempfile.mktemp(
                suffix=".ma", prefix="incompatible_asset_")

        create_valid_maya_asset(cls.valid_path)
        create_invalid_maya_asset(cls.invalid_path)
        create_compatible_scene(cls.compatible_path)
        create_incompatible_scene(cls.incompatible_path)

    @classmethod
    def tearDownClass(cls):
        if os.path.exists(cls.valid_path):
            os.unlink(cls.valid_path)
        if os.path.exists(cls.invalid_path):
            os.unlink(cls.invalid_path)
        if os.path.exists(cls.compatible_path):
            os.unlink(cls.compatible_path)
        if os.path.exists(cls.incompatible_path):
            os.unlink(cls.incompatible_path)

    def setUp(self):
        pc.newFile(f=True)

    def check_nothing_passed(self):
        with self.assertRaises(RuntimeError):
            checks.check_compatibility()

    def check_current_with_compatible_path(self):
        pc.openFile(self.valid_path)
        self.assertTrue(checks.check_compatibility(path1=self.compatible_path))

    def check_current_with_compatible_ref(self):
        pc.openFile(self.valid_path)
        ref1 = mi.createReference(self.compatible_path)
        self.assertTrue(checks.check_compatibility(ref1=ref1))

    def check_path_with_compatible_path(self):
        self.assertTrue(checks.check_compatibility(
            path1=self.valid_path, path2=self.compatible_path))

    def check_path_with_compatible_ref(self):
        ref2 = mi.createReference(self.compatible_path)
        self.assertTrue(checks.check_compatibility(
            path1=self.valid_path, ref2=ref2))

    def check_ref_with_compatible_ref(self):
        ref1 = mi.createReference(self.compatible_path)
        ref2 = mi.createReference(self.valid_path)
        self.assertTrue(checks.check_compatibility(
            ref1=ref1, ref2=ref2))

    def check_current_with_incompatible_path(self):
        pc.openFile(self.valid_path)
        self.assertFalse(checks.check_compatibility(
            path1=self.incompatible_path))

    def check_current_with_incompatible_ref(self):
        pc.openFile(self.valid_path)
        ref1 = mi.createReference(self.incompatible_path)
        self.assertFalse(checks.check_compatibility(ref1=ref1))

    def check_path_with_incompatible_path(self):
        self.assertFalse(checks.check_compatibility(
            path1=self.valid_path, path2=self.incompatible_path))

    def check_path_with_incompatible_ref(self):
        ref2 = mi.createReference(self.incompatible_path)
        self.assertFalse(checks.check_compatibility(
            path1=self.valid_path, ref2=ref2))

    def check_ref_with_incompatible_ref(self):
        ref1 = mi.createReference(self.incompatible_path)
        ref2 = mi.createReference(self.valid_path)
        self.assertFalse(checks.check_compatibility(
            ref1=ref1, ref2=ref2))

    def check_with_invalid(self):
        self.assertFalse(checks.check_compatibility(
            path1=self.valid_path, path2=self.invalid_path))

    def check_no_feedback(self):
        result = checks.check_compatibility(
                path1=self.valid_path, path2=self.invalid_path)
        with self.assertRaises(ValueError):
            success, feedback = result

    def check_feedback(self):
        success, feedback = checks.check_compatibility(
                path1=self.valid_path, path2=self.incompatible_path)
        self.assertFalse(success)
        self.assertTrue(bool(feedback['unmatched_sets'][0]))


if __name__ == "__main__":
    unittest.main()
